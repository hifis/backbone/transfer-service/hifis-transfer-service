require 'apache2'

function auth_check_hook(r)
	-- apache debugging msg
	r:debug("lua: auth_check_hook entered")

	-- set newuser to "sub@iss" and query local mapping file
	local newuser=r.subprocess_env["OIDC_CLAIM_sub"] .. "@" .. r.subprocess_env["OIDC_CLAIM_iss"]
	local userfile = io.open("LOCATION_2_ON_FILE_SYSTEM/userfile.csv", "r")
	local luser = "nobody"
	for line in userfile:lines() do
		local remote_user, local_user = line:match("(.-),(.*)")
		if newuser == remote_user then
			luser=local_user
            -- put the authorized user into ENV variable MAPPED_USER
			r.subprocess_env["MAPPED_USER"] = luser
		end
	end
	userfile:close()
	if luser ~= "nobody" then
        -- set REMOTE_USER with local username for the access.log
		r.user = luser
		r:debug(string.format("remote user %s mapped to local user %s.",newuser, luser))
		return apache2.DECLINED
	else
		r:debug(string.format("remote user %s unknown, not authorized.",newuser))
		return 403
	end

	-- apache debugging msg
	r:debug("lua: auth_check_hook left")
    -- Here be dragons: this function MUST return DECLINED, otherwise, this hook phase is finished and the following hook registered by mpm-itk
    -- that performs setuid() and setgid() will not be called!
	return apache2.DECLINED
end
