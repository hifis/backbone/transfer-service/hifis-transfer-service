This module is a prototype for an apache2 module that implements the data integrity check in rough accordance with RFC 3230.
It has been tested against FTS with the KIT webFTS interface and since webFTS does not return an error when "compare checksums" is enabled for the transfer,
the apache instance seems to return the correct checksum in the correct format.

The module is compiled with 
```
sudo apxs -i -a -c mod_want_digest.c
```
on the target machine. Currently, there is no config for the module, it just works on the HTTP GET and HEAD requests.
FTS first transfers a file to the destination and at the end of the transfer makes a HEAD request with the additional "Want-Digest: ADLER32" header token.
Currently, ADLER32, MD5 and SHA-1 checksums are supported without q-values and just one of them at a time. For FTS, this is okay-ish for now.

TODO:
- implement a caching mechanism that calculates the checksum of a file on the fly for a PUT request (although it would be more correct to calculate the checksum from the file on disk)
- implement the q-value ranking mechanism as described in RFC 3230, such that the requesting entity can rank the importance of different checksums it wants for the file in question.
- implement a precalculation for all files on disk that are exposed to the internet(TM) in order to save time for large files. the checksums could be placed in a hidden directory .checksums in files like filename.md5, filename.sha and filename.adler32.
