2020-08-31 FTS, 15:00

<https://meeting.desy.de/FTSRoadmap310820>

Mihai, Andrea,

Tim, Paul, Patrick (DESY)

Uwe (HIFIS)

* Quick introduction of HIFIS and HIFIS Data transfer to Mihai, data transfer takes place as HTTP-TPC between dCache as active and a modified Apache server as passive endpoint. In case, data needs to be transferred between two Apache endpoints, multihop has to be specified invisibly to the end user (especially in webFTS).
* Mihai: FTS3-devel supports OIDC-tokens and can be integrated with Helmholtz AAI and accept the Let's Encrypt CA for ease of use. HIFIS is welcome to use FTS3-devel for testing. The production release is planned for autumn of this year.
* Paul: How are PRs taken into account for FTS3 development?
* Patrick: webFTS is for attracting users; the long term goal is automating workflows for data transfer between big sites, webFTS is mainly thought for sporadic transfers
* Paul: Testing will be undertaken in order to have numbers to present and demonstrate that the service is a viable internal alternative to other transfer services. In production it is expected be used with Rucio or at least with the CLI.
* For the initial phase, webFTS will be the judging point for users starting to use the service. It is planned to improve webFTS
* letsencryptauthorityx3.pem letsencryptauthorityx3.signing_policy are installed on FTS3

Required Actions:

* Tim: send details for integration with Helmholtz AAI to Mihai mihai.patrascoiu@cern.ch
* Mihai: look at integration of FTS3-devel with Helmholtz AAI as an OIDC-provider and Let's Encrypt as CA for SSL/TLS connections
* Andrea: look at integration of webFTS instance with Helmholtz AAI as OIDC-provider and Let's Encrypt as CA as well.

```
letsencryptauthorityx3.pem 
```

```
letsencryptauthorityx3.signing_policy
```